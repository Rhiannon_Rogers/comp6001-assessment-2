var map;
var markers = [];
var currentplace;

//------------------
//---MAP CREATION---
//------------------

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: -37.6862810, lng: 176.1657770 },
        zoom: 16,
        clickableIcons: false
    });

    google.maps.event.trigger(window, 'resize');
}

//--------------------------
//---MODIFY SEARCH VALUES---
//--------------------------

//Get GeoLocation From Location String
function codeAddress() {

    clearprofile();
    deleteMarkers();
    markers = [];

    document.getElementById("locationstitle").innerHTML = "";
    document.getElementById("locations").innerHTML = "";


    var address = document.getElementById('locationbox').value;
    var geocoder = new google.maps.Geocoder();
    var request = {
        address: address
    };
    if (address === null || address === "") {
        alert("Please Enter a valid Address");
    } else {
        geocoder.geocode(request, verifyAddress);
    }

}

//Verify Address is Valid and Initiate Search
function verifyAddress(results, status) {
    if (status == 'OK') {

        position = results[0].geometry.location;
        map.setCenter(position);
        getPlaces(position.lat(), position.lng());

    } else {
        alert("Search was not successful for the following reason: " + status);
    }
}

//--------------------
//---PERFORM SEARCH---
//--------------------

//Get List of Places
//Issue 4 - Add maxPriceLevel as a search paramater.
function getPlaces(lat, lng) {
    var location = new google.maps.LatLng(lat, lng);

    var searchrequest = {
        location: location,
        radius: document.getElementById('distancesearch').value,
        type: ['restaurant'],
        maxPriceLevel: document.getElementById("pricesearch").value
    };

    service = new google.maps.places.PlacesService(map);
    service.nearbySearch(searchrequest, createMarkers);
}

//--------------------------------------
//---CREATE MARKERS AND LOCATION LIST---
//--------------------------------------

//Create All Markers And Display Locations List
function createMarkers(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {

        for (var i = 0; i < results.length; i++) {
            markers[i] = createMarker(results[i]);
            displayLocation(results[i], markers[i]);
        }

        document.getElementById("locationstitle").innerHTML = "<label class='goldback gap indent'>Search Results</label>";
        alert('Search completed successfully');
    } else {
        alert("Search was not successful for the following reason: " + status);
    }
}

//Create Each Individual Marker With Listener
function createMarker(place) {
    var position = place.geometry.location;

    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
    });

    google.maps.event.addListener(marker, 'click', function() {
        selectMarker(place, marker);
    });

    return marker;
}

//Display Each Location In Locations List
function displayLocation(place, marker) {
    var position = place.geometry.location;
    var button = document.createElement("Button");
    button.innerHTML = place.name;
    button.className = "btn btn-warning btn-block nomargin";
    button.addEventListener("click", function() {
        selectMarker(place, marker);
    });

    document.getElementById("locations").appendChild(button);
}

//------------------------------
//---SELECTED LOCATION EVENTS---
//------------------------------

//Select Current Marker
//Issue 2 - 'selectMarker' function updated to better work with 'resetMarkers' function
function selectMarker(place, marker) {
    resetMarkers(marker);
    getDetails(place.place_id);
    map.panTo(marker.position);
}

//Get Details For Selected Location/Place
function getDetails(placeID) {
    var request = {
        placeId: placeID
    };

    service.getDetails(request, displayDetails);
}

//Display Place Details
function displayDetails(place, status) {
    clearprofile();
    currentplace = place;
    //Issue 1 - added if statments to prevent coflict
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        if (place.photos != undefined) {
            document.getElementById("displaytext").innerHTML = "<img src='" + place.photos[0].getUrl({ 'maxWidth': 200, 'maxHeight': 200 }) + "' id='0' alt='Location Photo' class='displayphoto'></img>";
            // Issue 3 - Add next image button
            document.getElementById("displaytext").innerHTML += "<p class='displaylink'><button class='btn btn-warning btn-block' onclick='nextImage()'>Next Image</button></p>";
        }
        if (place.name != undefined) {
            document.getElementById("displaytext").innerHTML += "<p class='displaytitle'>" + place.name + "</p>";
        }
        if (place.rating != undefined) {
            document.getElementById("displaytext").innerHTML += "<p class='displayrating'>" + place.rating + "<img src='images/goldstar.png' class='goldstar' /> </p>";
        }
        if (place.formatted_address != undefined) {
            document.getElementById("displaytext").innerHTML += "<p class='displaylink'>" + place.formatted_address + "</p>";
        }
        if (place.formatted_phone_number != undefined) {
            document.getElementById("displaytext").innerHTML += "<p class='displaylink'><img src='images/phone.png' class='goldstar' />" + place.formatted_phone_number + "</p>";
        }
        if (place.website != undefined) {
            document.getElementById("displaytext").innerHTML += "<p class='displaylink'><a href='" + place.website + "' target='blank'><button class='btn btn-warning btn-block'>Website</button></a></p>";
        }
        if (place.reviews != undefined) {
            document.getElementById("reviewtitle").innerHTML = "<label class='reviewtitle goldback indent'>Reviews</label>";

            div = document.getElementById("reviews");
            div.innerHTML = "";

            for (var i = 0; i < place.reviews.length; i++) {
                div.innerHTML += "<h6>" + place.reviews[i].rating + "<img src='images/goldstar.png' class='goldstar2' />  " + place.reviews[i].author_name + "</h6>";
                div.innerHTML += "<p>" + place.reviews[i].text + "</p>";
            }
        }
        alert("Place details loaded successfully");
    } else {
        alert("Details could not be loaded for the following reason: " + status);
    }
}

// Issue 3 - onclick function for next image button.
function nextImage() {
    console.log(currentplace.name);
    var img = document.getElementById("displaytext").getElementsByTagName("img")[0];
    var p = parseInt(img.id);
    if (p < currentplace.photos.length - 1) {
        p++;
        console.log(p);
        img.src = currentplace.photos[p].getUrl({ 'maxWidth': 200, 'maxHeight': 200 });
        img.id = p;
    } else if (p == currentplace.photos.length - 1) {
        p = 0;
        img.src = currentplace.photos[p].getUrl({ 'maxWidth': 200, 'maxHeight': 200 });
        img.id = p;
    }
}

//---------------------------------
//---RESET OR DELETE ALL MARKERS---
//---------------------------------

//Reset Marker Animations
//Issue 2 - 'resetMarkers' function updated to better decide which markers should have their animation reset, and which marker to make bounce
function resetMarkers(marker) {
    for (var i = 0; i < markers.length; i++) {
        if (markers[i] == marker) {
            markers[i].setAnimation(google.maps.Animation.BOUNCE);
        } else {
            markers[i].setAnimation(null);
        }
    }  
}

//Delete Markers
function deleteMarkers() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
}

//---------------------------
//---DISPLAY SLIDER VALUES---
//---------------------------

// Display Distance Slider Values
//Issue 4 - Update the below function's name to prevent ambiguity with the new function 'searchPrice'.
function searchDistance() {
    var slidervalue = document.getElementById("distancesearch").value;
    document.getElementById("distancelabel").innerHTML = "Search Radius:";
    document.getElementById("distancelabel").innerHTML += " " + slidervalue + "m";
}

//Issue 4 - Add 'searchPrice' function to display the label values for the max price slider.
function searchPrice() {
    var slidervalue = document.getElementById("pricesearch").value;
    document.getElementById("pricelabel").innerHTML = "Max Price: $";
    var dollars = "$";

    for (var i = 0; i < slidervalue; i++) {
        document.getElementById("pricelabel").innerHTML += "$";
    }
}

//----------------------------
//-----CLEAR PROFILE INFO-----
//----------------------------

function clearprofile() {
    document.getElementById("displaytext").innerHTML = "";
    document.getElementById("reviewtitle").innerHTML = "";
    document.getElementById("reviews").innerHTML = "";
}