# Deviations From Wireframes
----
## Review Pictures

In the wireframe each place review had a picture to the left of it, but when we got to coding we learned that the API does not provide you with the profile pictures of the users that posted the reviews. This meant that we could not include images with our reviews, and had to restructure the layout of each individual review to just be text instead.

----
## Reviews Search Field

As above, we learned that you cannot actually specify minimum rating as a search parameter using the google maps API. You can specify other search terms, such as minimum and maximum price, but you cannot specify a minimum rating from reviews. It would have been possible to filter each result provided by the API to only display results with a specified rating, however, that would have required us to perform a place details request on every result. This was simply not viable, as it would take a long time to process, be very resource intensive, and eat up our API key (which only allows so many place details and search requests per day).  As a result, we had to remove the reviews search field entirely.

----
## List of Locations Pictures

Simiarly to the review pictures mentioned above, the list of the locations on the left hand side was originally going to include a picture next to each location name. However, we later learned that the API does not provide you with an image for each location, unless you do a seperate place details request for each and every location. Similarly to the reviews search field explanation above, this also would have been very resource intensive and eaten up our API key, and so we decided to not include pictures in the locations list in the end.

----
## Distance Slider

On our wireframes the distance search was originally a search box, and not a slider. However, we later decided that a slider works far better, as it prevents the user from typing in strange input (not numbers), and proved far quicker and easier for the user to use.

----
## Search Button

Originally our application was going to perform an automatic search on all input typed by the user. When implemented however, we discovered that it instantly ate up our API key (which only allows so many searches per day), and so we had to implement a search button instead.

----
## Positioning of Location Details

On our wireframes, when the details of a selected location are displayed there is a picture on the left, and the information on the right. When we implemented this in our application however, we realised that this made the image small and difficult to see, and the text became cramped. Because of this, we decided it would be better to stack the image and the text on top of each other instead, to make it easier to view.

----
## Adjustments to Portrait View

Similarly to above, the portrait view of our application originally had the search fields and the list of locations side by side, as well as the location details and the reviews side by side. When implemented however, this also looked very cramped and difficult to read. As a result, we decided to stack everything on top of each other instead, to make it easier to view.