# GUI Application Report
----
## Role Allocations

The groupís team leader is Rhiannon. 

As discussed in class, Johnathon will be primarily completing the front-end of the application, and in doing so, will be creating both the CSS and the HTML files for the application. In addition, Johnathon will also be implementing the bootstrap framework. As such, it will be Johnathonís job to work on the overall appearance of our application, as well as make the application responsive to different screen sizes.

Contrastingly, Rhiannon will be primarily completing the back-end of the application. As such, Rhiannon will be mostly focusing on the construction and implementation of the javascript files that focus on object events, and will be responsible for ensuring that the objects included in the application are fully functional.

When it comes to the implementation of the API itself, both Rhiannon and Johnathon will be working on this together. As this will be a new skill, it is important that everyone is able to gain the same understanding on how to implement an API, as well as help each other out during the learning phase.

----
## Communication Platforms

Our primary communication platform is Facebook Messenger. As our team checks Facebook Messenger on a regular basis, they are more likely to see and respond to any written communication posted there. 

In addition, we will also be using the Slack channel that has been set up for us as part of the assessment. In regards to this, we will be using our Slack channel to keep in contact with the tutor about any concerns or issues we may have.

Alongside Facebook and Slack, we will also be having regular meetings during class time, in order to discuss progress face to face. This is important, as there are some problems and issues that are much more easily solved in person. 

We believe that all 3 methods of communication will be very important in allowing us to communicate easily and effectively, as well as progress well in the completion of our application.

----
## Resources
### APIs

**Google Maps** - For the displaying of maps and landmarks. It also allows for directions to selected food outlets. -  [Link](https://developers.google.com/maps/)

**Google Places** - This is for displaying food places close to the users specified location : [Link](https://developers.google.com/places/)

### Documentation

**Electron** - A reference for information on our framework : [Link](https://electron.atom.io/docs/)

**Javascript** - A reference / refresher for our javascript : [Link](https://www.w3schools.com/js/)

**Bootstrap** - A reference for bootstrap : [Link](https://getbootstrap.com/docs/4.0/getting-started/introduction/)

----
## Tools

**Facebook Messenger** - for communication we will be using Facebook Messenger. It allows us to send files and share code, and all members of the team are familiar with it. [Link](http://www.facebook.com)

**Visual Studio Code** - this will be our primary development enviroment: [Link](http://code.visualstudio.com/)

**NPMJS** - The Javascript Package manager, which will allow us to install and get packages for electron. [Link](https://docs.npmjs.com/)

**Electron** - This is the framework we will be using to create our applications. [Link](https://electron.atom.io/)

**Git** - Either Git bash or Git Cmd, we will use this to update our repository and manage our project - [Link](https://git-scm.com/)

**Bitbucket** - we will be using bitbucket to store our project repository - [Link](http://bitbucket.org)

**Bootstrap** - We will be using bootstrap to help with the layout of our application - [Link](https://getbootstrap.com/)

----
## Wireframes

### Portrait Layout of Homepage

![Portrait Layout of Homepage](Portrait.png)

### Landscape Layout of Homepage

![Landscape Layout of Homepage](Landscape.png)
